document.addEventListener("DOMContentLoaded", function(e) {
    let tabs = loadAllTabs(); // load allNotes list from the local storage

    let notes = [];
    tabs.forEach((element, tab) => {
        element.forEach((item) => {
            let newNote = new Note(item.id, item.text, item.color, item.position.x, item.position.y);
            notes.push(newNote);
        });
        allTabs.set(tab, notes);
        notes = [];
    });
    console.log(allTabs);
    renderTabsButtons();

    printNotes(allTabs); // show the new note in main page
});

//allTabs
let allTabs = new Map();

let deafultTabe = 1;


function opentab(evt, tab) {
    deafultTabe = tab[3];
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}

// defult values from user 
let defultNoteColor = "#EFEFEF";





/**
 * array returned function that loads the allNotes from the local storage. 
 */
function loadAllTabs() {
    const storedData = localStorage.getItem("allTabs");
    console.log(new Map(JSON.parse(storedData)));
    return storedData ? new Map(JSON.parse(storedData)) : new Map();
};

/**
 * void returned function that recived an array and load it to local Storage.
 * @param {Array} recivedList 
 */
function saveToLocalStorage(recivedList) {
    console.log(recivedList);
    const jsonString = JSON.stringify(Array.from(recivedList.entries()));
    localStorage.setItem("allTabs", jsonString);
}

class Note {
    /**
     * unparametrized constructor that construct the id which is the date of make the note
     * also it construct the note's text and gave it an initial defult value 
     */

    constructor(id, text, color, positionx, positiony) {
        this.id = id;
        this.text = text;
        this.color = color;
        this.position = {
            x: positionx,
            y: positiony
        }
    }

    /**
     * getter function that return the text of the note
     */
    getText() {
        return this.text;
    }

    /**
     * setter function that recive the note's text as a string and assign it to the note
     * @param {strign} recivedText 
     */
    setText(recivedText) {
        this.text = recivedText;
    }

    /**
     * getter function that return the color of the note
     */
    getColor() {
        return this.color;
    }

    /**
     * setter function that recive the color as a string and assign it to the note
     * @param {string} recivedColor 
     */
    setColor(recivedColor) {
        this.color = recivedColor;
    }

    /**
     * getter function that return note's id
     */
    getId() {
        return this.id;
    }

    /**
     * getter function that return note's position in x and y axis
     */
    getPosition() {
        return this.position;
    }

    /**
     * setter function that recive the position as a x and y integer 
     * and assign it to the position
     * @param {int,int} recivedPosition 
     */
    setPosition(recivedPosition) {
        this.position.x = recivedPosition.x;
        this.position.y = recivedPosition.y;
    }
}
/**
 * void function that makes new empty note and present note the main page
 * and it stores the new note in the allNotes list as a Note object.
 * Also, it restore the list in the local storage.
 */
function addNote() {
    // make new empty note and store it in allNotes list
    let id = Date.now();
    let note = new Note(id, "write note...", defultNoteColor, 0, 0);
    allTabs.get("tab" + deafultTabe).push(note);
    let htmlNote = toHTMLNote(note);
    document.getElementById("tab" + deafultTabe).innerHTML += (htmlNote);

    saveToLocalStorage(allTabs); // load all tabs to the local stoeage

}
/**
 * void function that print recived list in the main page
 * @param {Array} recivedList 
 */
function printNotes(recivedList) {

    recivedList.forEach((element, tab) => {
        document.getElementById(tab).innerHTML = ""; //reset element's value
        element.forEach(note => {
            let htmlNote = toHTMLNote(note);
            document.getElementById(tab).innerHTML += (htmlNote);
        });
    });


}
/**
 * function that recives note's object and it converts it to html div
 * and returns as a string 
 * @param {Note} note 
 */

function renderTabsButtons() {
    let tabsButtons = "";
    let boards = "";
    allTabs.forEach((element, tab) => {
        boards = boards + ` <div id="${tab}" class="tabcontent">

        </div>`;
        tabsButtons = tabsButtons + `<button id="${tab+"button"}" class="tablinks" onclick="opentab(event, '${tab}')">${tab}</button>`;
    });

    document.getElementsByClassName("tabbutton")[0].innerHTML = tabsButtons;
    document.getElementsByClassName("all-tab")[0].innerHTML = boards;
    document.getElementById("tab1").style = " display:block;";

}

function toHTMLNote(note) {

    let noteDiv = `
    <div class="resize">
    <div class="note" id="${note.id}" style="background-color: ${note.getColor()}; ">
                <p class="text" id="${"text" + note.getId()}" >
                    ${note.getText()}
                </p>
                <div class="bottom">
                    <div class="colors">
                        <div onclick ="changeolor(${note.id},'white')" class="white" id="${"white" + note.getId()}"></div>
                        <div onclick ="changeolor(${note.id},'#feaeae')" class="red" ></div>
                        <div onclick ="changeolor(${note.id},'#cdfcb6')"  class="green" ></div>
                        <div onclick ="changeolor(${note.id},'#b6d7fc')"  class="blue" ></div>
                    </div>
                    <div class="delete" id="${"delete" + note.getId()}" onclick="deleteNote(${note.getId()})">
                        <span> &#128500; </span>
                    </div>
                </div>
            </div>
            </div>`;
    return noteDiv;
}

function changeolor(id, color) {
    // let noteIndex = allNotes.findIndex((note) => note.id == noteId);
    allTabs.get("tab" + deafultTabe).forEach(element => {
        if (element.id == id) {
            element.color = color;
        }

    });

    printNotes(allTabs); // show the new note in main page
    saveToLocalStorage(allTabs);

}

/**
 * this function recives the id of note that will be deleted
 * @param {number} noteId
 */
function deleteNote(noteId) {
    let noteIndex = allTabs.get("tab" + deafultTabe).findIndex((note) => note.id == noteId);
    allTabs.get("tab" + deafultTabe).splice(noteIndex, 1);
    saveToLocalStorage(allTabs);
    printNotes(allTabs);
};

// create tabe
function createTabe() {
    let tab = allTabs.size + 1;
    allTabs.set("tab" + tab, new Array());
    renderTabsButtons();
    printNotes(allTabs);
}